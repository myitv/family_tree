<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/29
 * Time: 22:14
 */

namespace app\portal\controller;
use think\controller\Rest;
use think\Db;

class FamilyPrologueController extends Rest
{
    public function get()
    {
        $data = Db::name('prologue')->find();
        if($data){
            return cmf_api_json(true,html_entity_decode($data['detail']),'成功');
        }else{
            return cmf_api_json(false,'','没有数据');
        }
    }
}