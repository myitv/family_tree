<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/28
 * Time: 22:13
 */

namespace app\portal\controller;

use think\Request;
use app\portal\model\EpochModel;
use think\controller\Rest;

class FamilyEpochController extends Rest
{
    /**
     * 获取不含详情的朝代列表
     * @return \think\response\Json
     */
    public function getListSimple()
    {
        $model_epoch = new EpochModel();
        $data = $model_epoch->getListSimple();
        if($data){
            return cmf_api_json(true,$data->toArray());
        }else{
            return cmf_api_json(false,[],'没有数据');
        }
    }

    /**
     * 获取含详情的朝代列表
     * @return \think\response\Json
     */
    public function getListComplex()
    {
        $model_epoch = new EpochModel();
        $data = $model_epoch->getListComplex();
        if($data){
			foreach($data as $k => $v){
				// $data[$k]['detail2'] = htmlspecialchars_decode($v['detail']);
				$data[$k]['detail'] = html_entity_decode($v['detail']);
			}
            return cmf_api_json(true,$data->toArray());
        }else{
            return cmf_api_json(false,[],'没有数据');
        }
    }

    public function getById()
    {
        $model_epoch = new EpochModel();
        $id = input('id/d');
        if(!$id){
            return cmf_api_json(false,[],'参数错误');
        }
        $data = $model_epoch->getById($id);
        if($data){
            return cmf_api_json(true,$data->toArray());
        }else{
            return cmf_api_json(true,[],'没有数据');
        }

    }

}