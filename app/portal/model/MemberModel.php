<?php
/**
 * Created by PhpStorm.
 * User: 珊珊
 * Date: 2018/7/12
 * Time: 21:45
 */

namespace app\portal\model;
use think\Model;
use think\Db;

class MemberModel extends Model
{
    /**
     * @param $parent
     */
    public function memberTree($parent, $generation,$deep, $return = [], $currentDeep = 1)
    {

        $res = Db::name('member')->where(['parent_id' => $parent, 'is_deleted' => 0,'generation'=>$generation+1,'gender'=>1])->field('id,name,gender,ranking,parent_id,generation,false as has_child')->select();
        if ($res) {
            $res = $res->toArray();
            //把return数组 加入 has_child
            if($res) {
                foreach ($return as $key => $vo) {
                    if (!$vo['has_child']) {
                        $return[$key]['has_child'] = false;
                    }
                    if ($vo['id'] == $parent) {
                        $return[$key]['has_child'] = true;
                    }
                }
            }
            if ($deep < $currentDeep) {
                return $return;
            }
            foreach ($res as $item) {
                $item['has_child'] = false;
                $return[] = $item;
                $return = $this->memberTree($item['id'], $generation+1,$deep, $return, $currentDeep + 1);
            }
            return $return;
        } else {
            return $return;
        }
    }
}