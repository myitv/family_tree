<?php
/**
 * Created by PhpStorm.
 * User: 珊珊
 * Date: 2018/6/3
 * Time: 22:31
 */

namespace app\admin\model;
use think\Model;
use think\db;

class ClanModel extends Model
{
    /**
     * @param $parent
     */
    public function clanTree($parent,$return = [])
    {
        $res = Db::name('clan')->where(['parent_id'=>$parent,'is_deleted'=>0])->select();
        if($res){
            foreach($res as $item){
                $return[] = $item;
                $return = $this->clanTree($item['id'],$return);
            }
            return $return;
        }else{
            return $return;
        }
    }
	
	/**
     * @param $parent
     */
    public function clanTreeTwo($parent,$return = [])
    {
        $res = Db::name('clan')->where(['parent_id'=>$parent,'level' => ['<', 2],'is_deleted'=>0])->select();
        if($res){
            foreach($res as $item){
                $return[] = $item;
                $return = $this->clanTreeTwo($item['id'],$return);
            }
            return $return;
        }else{
            return $return;
        }
    }

    public function insertClan($name,$detail,$parent_id)
    {
        $data = [];
        //先计算level
        if($parent_id == 0){
            $level = 0;
        }else{
            $rs = $this->where(['id'=>$parent_id])->find()->toArray();
            $level = $rs['level'] +1;
        }
        $data['level'] = $level;
        $data['create_time'] = $data['update_time'] = time();
        $data['name'] = $name;
        $data['detail'] = $detail;
        $data['parent_id'] = $parent_id;
        return $this->insertGetId($data);
    }

    public function getListByName($name)
    {
        return $this->where(['name'=>['like','%'.$name.'%']])->select()->toArray();
    }

    public function updateById($id,$data)
    {
        return $this->where(['id'=>$id])->update($data);
    }

    public function getClanArray()
    {
        $res = $this->select()->toArray();
        return array_combine(array_column($res,'id'),array_column($res,'name'));
    }

    /**
     * 根据id获取详情（含上级房族名称）
     * @param $id
     */
    public function getById($id)
    {
        $res = $this->where(['id'=>$id,'is_deleted'=>0])->find()->toArray();
        if($res){
            if($res['parent_id']){
                $clan_name = $this->getClanArray();
                $res['parent_name'] = $clan_name[$res['parent_id']];
            }else{
                $res['parent_name'] = '顶级房族';
            }
            return $res;
        }else{
            return [];
        }
    }

    /**
     * 循环的删除
     */
    public function cyclicDelete($id)
    {
        $data = $this->where(['id'=>$id,'is_deleted'=>0])->find();
        if($data){
            $data = $data->toArray();
            $this->where(['id'=>$id,'is_deleted'=>0])->update(['is_deleted'=>1]);
            $children = $this->where(['parent_id'=>$data['id'],'is_deleted'=>0])->select();
            if($children){
                $children = $children->toArray();
                foreach ($children as $child) {
                    $this->cyclicDelete($child['id']);
                }
            }
        }
    }
}