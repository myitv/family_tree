<?php

namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Request;
use think\db;

class EpochController extends AdminBaseController
{
    public function listing(){
        $name = input('get.name/s');
        if($name){
            $data = Db::name('epoch')->where(['name'=>['like','%'.$name.'%'],'is_deleted'=>0])->select();
        }else{
            $data = Db::name('epoch')->where(['is_deleted'=>0])->select();
        }
        $this->assign('data',$data);
        $this->assign('name',$name);
        return $this->fetch();
    }

    public function add(){
        if(Request::instance()->isPost()){
            $insert = [
                'name'=>input('post.name/s'),
                'detail'=>input('post.introduction/s'),
                'create_time'=>time(),
                'update_time'=>time(),
                'is_deleted'=>0
            ];
            if(!trim($insert['name'])){
                $this->error('朝代名称不能为空');
                exit;
            }
            if(!trim($insert['detail'])){
                $this->error('朝代描述不能为空');
                exit;
            }
            $rs = Db::name('epoch')->insert($insert);
            if($rs){
                $this->success('添加成功');
            }else{
                $this->error('添加失败');
            }
        }else{
            return $this->fetch();
        }
    }

    public function edit(){
        $id = input('id/d');
        if(!Request::instance()->isPost()) {
            if ($id) {
                $data = Db::name('epoch')->where(['id' => $id, 'is_deleted' => 0])->find();
                $this->assign('detail', $data);
                return $this->fetch('add');
            } else {
                $this->error('参数错误');
            }
        }else{
            //表单提交
            if ($id) {
                $insert = [
                    'name' => input('post.name/s'),
                    'detail' => input('post.introduction/s'),
                    'update_time' => time(),
                ];
                if (!trim($insert['name'])) {
                    $this->error('朝代名称不能为空');
                    exit;
                }
                if (!trim($insert['detail'])) {
                    $this->error('朝代描述不能为空');
                    exit;
                }
                //
                if(!Db::name('epoch')->where(['id'=>$id,'is_deleted'=>0])->find()){
                    $this->error('朝代不存在');
                }
                $rs = Db::name('epoch')->where(['id'=>$id])->update($insert);
                if($rs){
                    $this->success('编辑成功');
                }
            }else{
                $this->error('参数错误');
            }
        }
    }

    public function delete(){
        $id = input('id/d');
        if($id) {
            if (Db::name('epoch')->where(['id' => $id])->delete()) {
                $this->success('删除成功');
            }
        }
    }

    public function view(){
        $id = input('id/d');
        if($id){
            if(!Db::name('epoch')->where(['id'=>$id,'is_deleted'=>0])->find()){
                $this->error('朝代不存在');
            }
            $data = Db::name('epoch')->where(['id'=>$id,'is_deleted'=>0])->find();
            $this->assign('detail',$data);
            return $this->fetch();
        }else{
            $this->error('参数错误');
        }
    }
}