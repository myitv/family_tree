<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/5/26
 * Time: 16:49
 */

namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use think\Request;
class PrologueController extends AdminBaseController
{
    public function _initialize(){
        parent::_initialize();
    }


    public function index(){
        $request = Request::instance();
        $res = Db::name('prologue')->where(['id' => 1])->find();
        if($res){
            $this->assign('prologue',$res['detail']);
        }else{
            $this->assign('prologue','');
        }
        if($request->isPost()){
//            print_r($cb = request()->post());die;
            $txt = input('post.prologue/s');
            if(Db::name('prologue')->where(['id'=>1])->find()){
                Db::name('prologue')->where(['id' => 1])->update(['detail' => $txt, 'update_time' => time()]);
            }else{
                Db::name('prologue')->insert(['detail' => $txt, 'update_time' => time(),'create_time'=>time()]);
            }
            $this->success('保存成功');
        }
        return $this->fetch();
    }

    public function uploadImg()
    {
        $file = request()->file('upload');
        $cb = request()->get('CKEditorFuncNum'); //获得ck的回调id
        if ($file) {
            $info = $file->move(ROOT_PATH . 'public' . DS . 'upload');
            $file_name = 'http://'.$_SERVER['HTTP_HOST'].'/upload/' . $info->getSaveName();
//                exit(ajax_list_response(0,0,['src'=>$file_name]));
            $file_name = str_replace('\\','/',$file_name);
//            {"uploaded" : 1, "fileName" : "image.png", "url":"http://localhost:15593/UploadImages/RickEditorFiles/Content/2017-05-23 10_39_54.png"  }
            $arr = ['uploaded'=>1,'fileName'=>'image','url'=>$file_name];
            exit(json_encode($arr));
        }
    }
}