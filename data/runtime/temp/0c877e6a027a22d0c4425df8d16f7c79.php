<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:53:"themes/admin_simpleboot3/admin\member\member_add.html";i:1536333373;s:70:"E:\zupu\family_tree\public\themes\admin_simpleboot3\public\header.html";i:1536333373;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo \think\Request::instance()->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
<div class="wrap">
    <h1><?php echo $type==1?'添加子女':($type==2?'添加配偶':''); ?></h1>
    <form class="form" id="form" method="post" action="">
        <input type="hidden" name="type" value="<?php echo $type; ?>">
        <input type="hidden" name="id" value="{isset($id)?$id:''}">
        <input type="hidden" name="target_id" value="{isset($target_id)?$target_id:''}">
        <div class="form-group">
            <label for="name">名</label>
            <input type="text" class="form-control" id="name" name="name" maxlength="30" value="<?php echo isset($data)?$data['name']:''; ?>" placeholder="本氏族的成员，无需填姓">
        </div>
        <?php if($type==1): ?>
        <div class="form-group">
            <label for="styleName">字</label>
            <input type="text" class="form-control" id="styleName" name="style_name" maxlength="30" value="<?php echo isset($data)?$data['style_name']:''; ?>" placeholder="字，可为空">
        </div>
        <div class="form-group">
            <label>性别</label>
            <div class="radio">
                <label>
                    <input type="radio" name="gender" id="gender0" value="0" <?php if(!isset($data) || (isset($data) && $data['gender']==0)): ?>checked<?php endif; ?>>
                    未知
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="gender" id="gender1" value="1" <?php if(isset($data) && $data['gender']==1): ?>checked<?php endif; ?>>
                    男
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="gender" id="gender1" value="2" <?php if(isset($data) && $data['gender']==2): ?>checked<?php endif; ?>>
                    女
                </label>
            </div>
        </div>
            <!--添加子女 -->
            <div class="form-group">
                <label for="clan">房族</label>
                <select name="clan_id" id="clan" class="form-control">
                    <option value="">请选择...</option>
                    <option value="0" <?php if(isset($data) && $data['clan_id']==0): ?>selected="selected"<?php endif; ?>>无</option>
                    <?php if(is_array($clan_tree) || $clan_tree instanceof \think\Collection || $clan_tree instanceof \think\Paginator): if( count($clan_tree)==0 ) : echo "" ;else: foreach($clan_tree as $key=>$vo): ?>
                        <option value="<?php echo $vo['id']; ?>" <?php if(isset($data) && $data['clan_id']==$vo['id']): ?>selected="selected"<?php endif; ?>><?php $__FOR_START_237__=0;$__FOR_END_237__=$vo['level'];for($i=$__FOR_START_237__;$i < $__FOR_END_237__;$i+=1){ ?>|-<?php } ?><?php echo $vo['name']; ?></option>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="ranking">家庭排行</label>
                <input type="number" class="form-control" id="ranking" name="ranking" maxlength="30" value="<?php echo isset($data)?$data['ranking']:''; ?>" placeholder="家庭排行">
            </div>
        <?php endif; ?>
        <div class="form-group">
            <label for="description">成员介绍</label>
            <textarea class="form-control" id="description" row="3" name="description"><?php echo isset($data)?$data['description']:''; ?></textarea>
        </div>
        <button class="btn btn-primary" type="submit">确定</button>
    </form>
</div>
<script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
<script type="application/javascript" language="javascript">
    $().ready(function() {
        $("#form").validate({
            rules:
            {
                name: "required",
                gender:"required"<?php if($type==1): ?>,
                clan_id:{
                    required:true
                },
                ranking:{
                    required:true,
                    digits:true,
                    min:1
                }
                <?php endif; ?>
            },
            messages:
            {
                name: "请输入名字",
                gender:"请选择性别"<?php if($type==1): ?>,
                clan_id:{
                    required:"请选择房族"

                },
                ranking:{
                    required:"请选择输入家庭排行",
                    digits:"必须是正整数",
                    min:"最小值为1"
                }
                <?php endif; ?>
            }
        });
    });
</script>
</body>
</html>